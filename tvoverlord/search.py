import sys
import subprocess
import os
import platform
# noinspection PyUnresolvedReferences
from pprint import pprint as pp

from urllib import parse, request, error
import click
from xml.etree import ElementTree
from datetime import datetime
from datetime import timezone

from tvoverlord.config import Config
from tvoverlord.util import U
import tvoverlord.tvutil as tu


class JackettSearch:
    """Search using Jackett"""
    JACKETT_URL = 'http://localhost:9117/api/v2.0/indexers/all/results/torznab'
    JACKETT_APIKEY = '2792yz0lz1xnck7j51y508d19kp3cq7h'

    def search(self, search_string: str, season=False, episode=False,
               date_search=None, search_type='torrent'):

        search_string = search_string.rstrip()
        se_ep = tu.sxxexx(season, episode)
        title_season = f'{search_string} {se_ep}'.strip()

        click.echo()
        click.echo(f'{title_season}...')

        header = [[search_string, ''],
                  ['Torrent Name', 'Size', 'Age', 'Seeds', 'Indexer'],
                  [0, 10, 9, 6, 14],
                  ['<', '>', '<', '>', '<']]

        query = {
            't': 'tvsearch',
            'q': title_season,
            'season': season if season else '',
            'ep': episode if episode else '',
            'apikey': self.JACKETT_APIKEY,
            'limit': 50,
        }
        full_url = '{}?{}'.format(self.JACKETT_URL, parse.urlencode(query))
        try:
            with request.urlopen(full_url) as response:
                encoding = response.headers.get_content_charset()
                xml_text = response.read().decode(encoding)
        except error.URLError:
            click.secho('Cannot connect to Jacket', fg='red')
            sys.exit(1)

        data = self.parse_xml(xml_text)
        data.sort(key=lambda x: int(x[3]), reverse=True)

        data = [header, data]

        click.echo('[%sA' % 3)

        return data

    @staticmethod
    def de_moji(text):
        """Remove non ascii characters from a string

        Mostly because some torrent names have emojis an other odd
        characters"""
        return text.encode('ascii', 'ignore').decode('ascii')

    def parse_xml(self, xml_text):
        """Parse the xml returned from Jackett"""

        data = []

        elements = ElementTree.fromstring(xml_text)
        for channel in elements:
            for item in channel:
                if item.tag != 'item':
                    continue

                torznabs = item.findall(
                    '{http://torznab.com/schemas/2015/feed}attr')
                ns_data = {}
                for torznab in torznabs:
                    ns_name = torznab.attrib['name']
                    ns_value = torznab.attrib['value']
                    ns_data[ns_name] = ns_value

                link_url = item.find('link').text
                enclosure_url = item.find('enclosure').attrib['url']
                try:
                    jackett_url = ns_data['magneturl']
                except KeyError:
                    jackett_url = 'NO MAGENTURL'

                pub_date = item.find('pubDate').text
                pub_date = datetime.strptime(
                    pub_date, '%a, %d %b %Y %H:%M:%S %z')
                now = datetime.now(timezone.utc)
                delta = now - pub_date
                if delta.days:
                    days = round(delta.days)
                    pretty_date = f'{days} days'
                elif delta.seconds > 3600:
                    hours = round(delta.seconds / 60 / 60)
                    pretty_date = f'{hours} hours'
                else:
                    minutes = round(delta.seconds / 60)
                    pretty_date = f'{minutes} min'

                title = self.de_moji(item.find('title').text)

                data.append([
                    title,
                    U.pretty_filesize(item.find('size').text),
                    pretty_date,
                    ns_data['seeders'],
                    item.find('jackettindexer').text,
                    enclosure_url
                ])
            self.sort_torrents(data)
            return data

    def sort_torrents(self, episodes):
        # sort by seeds
        episodes.sort(key=lambda x: int(x[3]), reverse=True)

        # Remove torrents with 0 seeds
        for i, episode in enumerate(episodes):
            seeds = int(episode[3])
            if not seeds:
                del episodes[i]

        # remove duplicates since different sites might
        # have the same torrent
        titles = []
        for i, episode in enumerate(episodes):
            title = episode[0]
            if title in titles:
                del episodes[i]
            else:
                titles.append(title)

        # remove duplicates based on the magnet hash
        hashes = []
        for i, episode in enumerate(episodes):
            o = parse.urlparse(episode[5])
            try:
                torrent_hash = parse.parse_qs(o.query)['xt']
                torrent_hash = torrent_hash[0].split(':')[-1]
                if torrent_hash in hashes:
                    del episodes[i]
                else:
                    hashes.append(torrent_hash)
            except KeyError:
                pass

    def download(self, chosen_show, destination, search_type='torrent'):
        magnet_link = chosen_show
        if chosen_show.startswith('http'):
            magnet_link = self.http_to_magnet(chosen_show)

        # write magnet links to a file
        if Config.magnet_dir:
            Config.magnet_dir = os.path.expanduser(Config.magnet_dir)
            fn = self.magnet_filename(magnet_link)
            if os.path.isdir(Config.magnet_dir):
                full = os.path.join(Config.magnet_dir, fn)
                with open(full, 'w') as f:
                    f.write(magnet_link)
            else:
                sys.exit('\n"%s" does not exist' % Config.magnet_dir)

        # use a command specified in config.ini
        elif Config.client:
            args = self.config_command(magnet_link)
            try:
                subprocess.Popen(args)
            except FileNotFoundError:
                sys.exit('\n"%s" not found.' % args[0])

        elif platform.system() == 'Linux':
            err_msg = tu.format_paragraphs('''
                You do not have a default handler for magnet
                links.  Either install a bittorent client or
                configure the "magnet folder" or "client"
                settings in config.ini.''')

            is_x = True if os.environ.get('DISPLAY') else False
            if is_x:
                app = 'xdg-open'
            else:
                click.echo()
                sys.exit(err_msg)

            try:
                subprocess.Popen([app, magnet_link],
                                 stderr=subprocess.DEVNULL,
                                 stdout=subprocess.DEVNULL)
            except OSError:
                click.echo()
                sys.exit(err_msg)

        elif platform.system() == 'Darwin':
            subprocess.Popen(["open", "--background", magnet_link])

        elif platform.system() == 'Windows':
            os.startfile(magnet_link)

        else:
            unknown_system = platform.platform()
            sys.exit(f'\nUnknown system: {unknown_system}')

        return ''

    @staticmethod
    def http_to_magnet(chosen_show):
        try:
            with request.urlopen(chosen_show).getUrl() as response:
                encoding = response.headers.get_content_charset()
                magnet_link = response.read().decode(encoding)
        except error.HTTPError as e:
            magnet_link = e.url
        return magnet_link

    def magnet_filename(self, chosen_show=None):
        se_ep = tu.sxxexx(self.season, self.episode)
        if se_ep:
            fullname = '%s %s.magnet' % (self.show_name, se_ep)
            fullname = fullname.replace(' ', '_')
        else:
            show_fname = self.show_name
            for f in self.episodes:
                if chosen_show == f[5]:
                    show_fname = tu.clean_filename(f[0], strict=True)
            fullname = '%s.magnet' % (show_fname)
        return fullname

    @staticmethod
    def config_command(chosen_show):
        args = [i.replace('{magnet}', chosen_show) for i in Config.client]
        if args == Config.client:
            sys.exit('\nNo {magnet} replacement flag was found in config.ini, client section.')
        return args


if __name__ == '__main__':
    pass
