import subprocess
import click


class Jackett:
    """Start and stop a Jacket server

    In a class's __del__ method, use the Jacket.stop() method to
    automaticaly terminate Jacket when done."""

    admin_url = 'http://localhost:9117'

    def __init__(self):
        jackett_dir = '/home/sm/bin-apps/Jackett'
        self.jackett_cmd = [f'{jackett_dir}/jackett']

    def start(self):
        """Start jackett and set a class var so it can be terminated later"""
        try:
            self.command = subprocess.Popen(
                self.jackett_cmd, bufsize=1, stdout=subprocess.PIPE,
                stderr=subprocess.DEVNULL, universal_newlines=True)
            for line in self.command.stdout:
                if 'Application started.' in line:
                    break

            click.echo(' '.join([
                click.style('Jackett started, admin url:', fg='bright_black'),
                click.style(f'{self.admin_url}', fg='bright_blue', underline=True),
                click.style(f'\n{self.jackett_cmd[0]}', fg='bright_black'),
            ]))
        except FileNotFoundError:
            self.command = False
            click.secho(f'Jackett not found: {self.jackett_cmd[0]}', fg='red')

    def stop(self):
        """Terminate the running Jacket server"""
        click.echo()
        if self.command:
            self.command.terminate()
            click.secho('Jackett stopped', fg='bright_black')
        else:
            click.echo()
            click.secho('Warning, could not stop Jackett', fg='black', bg='yellow')

    def __del__(self):
        self.stop()
