#!/usr/bin/env python
import os
import sys
import ctypes
import textwrap
import re
from urllib import parse
import click
import unicodedata
import string
# noinspection PyUnresolvedReferences
from pprint import pprint as pp
from collections import namedtuple

from tvoverlord.config import Config
from tvoverlord.util import U


def hash2magnet(magnet_hash, title):
    magnet_url = 'magnet:?xt=urn:btih:{}&dn={}'
    magnet = magnet_url.format(magnet_hash, parse.quote(title))
    return magnet


def sxxexx(season, episode):
    """Return a season and episode formated as S01E01"""
    se = ''
    if season and episode:
        season = str(season)
        episode = str(episode)
        if episode in ['all', 'complete', 'full']:
            # S03(all)
            episode = f'({episode})'
        else:
            # S03E09
            episode = f'E{episode:0>2}'
        se = 'S{season:0>2}{episode}'.format(
            season=season, episode=episode)
    elif season:
        # S03
        se = 'S{season:0>2}'.format(season=season)
    return se


def sxee(season, episode):
    """Return a season and episode formated as 1X01"""
    se = ''
    if season and episode:
        season = str(season)
        episode = str(episode)
        se = '{season}X{episode:0>2}'.format(
            season=season, episode=episode)
    elif season:
        se = '{season}Xall'.format(season=season)
    return se


def style(text, fg=None, bg=None, bold=None, strike=None, ul=None):
    if Config.is_win:
        fancy = click.style(text, fg=fg, bg=bg, bold=bold, underline=ul)
    else:
        fancy = U.style(text, fg=fg, bg=bg, bold=bold, strike=strike, ul=ul)
    return fancy


def dict_factory(cursor, row):
    """Changes the data returned from the db from a
    tupple to a dictionary"""
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


def format_paragraphs(msg, indent=''):
    if not msg:
        return ''
    paragraphs = re.split('\n\n+', msg)
    for i, paragraph in enumerate(paragraphs):
        paragraph = textwrap.dedent(paragraph).strip()
        paragraph = textwrap.fill(
            paragraph,
            initial_indent=indent,
            subsequent_indent=indent)
        paragraphs[i] = paragraph
    document = '\n\n'.join(paragraphs)
    return document


class FancyPrint:
    def __init__(self):
        self.blank_line = '\b' * Config.console_columns

    def standard_print(self, msg):
        self._back_print(msg)

    def done(self, msg=''):
        msg = msg.ljust(Config.console_columns)
        sys.stdout.write('\033[F')
        self._back_print(msg)

    def _back_print(self, msg):
        sys.stdout.write(self.blank_line)
        sys.stdout.write(msg)
        sys.stdout.flush()


# http://stackoverflow.com/a/6397492
def disk_info(path):
    """Return disk usage associated with path."""
    if Config.is_win:
        free_bytes = ctypes.c_ulonglong(0)
        ctypes.windll.kernel32.GetDiskFreeSpaceExW(
            ctypes.c_wchar_p(path), None, None,
            ctypes.pointer(free_bytes))
        return free_bytes.value
    else:
        st = os.statvfs(path)
        free = (st.f_bavail * st.f_frsize)
        total = (st.f_blocks * st.f_frsize)
        used = (st.f_blocks - st.f_bfree) * st.f_frsize
        try:
            percent = ret = (float(used) / total) * 100
        except ZeroDivisionError:
            percent = 0
        # NB: the percentage is -5% than what shown by df due to
        # reserved blocks that we are currently not considering:
        # http://goo.gl/sWGbH
        usage_ntuple = namedtuple('usage',  'total used free percent')
        # return usage_ntuple(total, used, free, round(percent, 1))
        return free


def itemize(shows):
    depth = len(str(len(shows))) + 1
    for i, show in enumerate(shows):
        i = i + 1
        number = '%s.' % i
        number = number.rjust(depth)
        click.echo('  %s %s' % (number, show.seriesname))


def clean_filename(fname, strict=False):
    if strict:
        valid_chars = '-_.(){alpha}{nums}'.format(
            alpha=string.ascii_letters, nums=string.digits)
    else:
        valid_chars = '-_.()[] {alpha}{nums}'.format(
            alpha=string.ascii_letters, nums=string.digits)
    fname = unicodedata.normalize('NFKD', fname)
    clean = ''.join(c for c in fname if c in valid_chars)
    return clean


if __name__ == '__main__':
    pass
